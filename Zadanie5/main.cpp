#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm>
#include <sstream>

using namespace std;

template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    int n = macierz.size();
    for(int i = 0; i < n; ++i){
        for(int j = 0 ; j < n; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
    cout << endl;
}

template <typename T>
void show_wektor(const vector<T> wektor){
    int n = wektor.size();
    for(int i = 0; i < n; ++i){
        cout << wektor[i] << " ";
    }
    cout << endl;
}

void fill_macierz(vector<vector<double>> & macierz, int n, vector<double> matrix_values){
    auto it = matrix_values.begin();
    for(int i = 0; i < n; ++i ){
        for (int j = 0; j < n; ++j){
            macierz[i].at(j) = *it;
            it++;
        }
    }
    show_macierz(macierz);
}

template <typename T>
void fill_macierz_jednostkowa(vector<vector<T>> &macierz, int n){
    for(int i = 0; i < n; ++i){
        macierz[i].at(i) = 1;
    }
    //show_macierz(macierz, n);
}


void split(string str, vector<vector<double>> &out_vector) {
    vector<double> values;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, ' ')) {
        if(!tok.empty())
            values.push_back(stod(tok));
    }
    out_vector.push_back(values);
}

static bool abs_compare(int a, int b)
{
    return (abs(a) < abs(b));
}

void swap_kolumny(vector<vector<double>> &macierz, int kolumna1, int kolumna2){
    int size = macierz.size();
    for(int i = 0; i < size; ++i){
        int tmp = macierz[i].at(kolumna2);
        macierz[i].at(kolumna2) = macierz[i].at(kolumna1);
        macierz[i].at(kolumna1) = tmp;
    }
}

int maksymalna_wartosc_bezwzgledna_wiersza(vector<vector<double>> macierz, int n){

    auto max2 = max_element(macierz[n].begin(), macierz[n].end(), abs_compare);
    int index = distance(macierz[n].begin(), max2);
    return index;
}

void metoda_doolittla_permutacja(vector<vector<int>> &macierz_permutacji, vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>> macierz_A){
    int n = macierz_A.size();
    for(int k = 0; k < n; k++) {
        // -- wyznaczanie k-tego wierza macierzy U
        for (int j = k; j < n; ++j) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }

        int index = maksymalna_wartosc_bezwzgledna_wiersza(macierz_U, k);
        for(int i = 0; i < n; ++i){
            auto tmp = macierz_permutacji[i].at(index);
            macierz_permutacji[i].at(index) = macierz_permutacji[i].at(k);
            macierz_permutacji[i].at(k) = tmp;
        }

        swap_kolumny(macierz_U, k, index);
        swap_kolumny(macierz_A, k, index);

        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }

    }
}

template <typename T, typename K>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> &macierz_L, vector<vector<K>> &macierz_U){
    int n = macierz_L.size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        for(int j = 0; j < n; ++j){
            double suma = 0;
            for(int p = 0; p < n; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    return result_macierz;
}

template <typename T>
vector<T> podstawianie_wprzod(vector<vector<T>> macierz_L, vector<T> wektor_b){

    int n = macierz_L.size();
    vector<T> wektor_y(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < i; ++j){
            suma += macierz_L[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/ macierz_L[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<T> podstawianie_wstecz(vector<vector<T>> macierz_U, vector<T> wektor_b){

    int n = macierz_U.size();
    vector<T> wektor_y(n);
    for(int i = n-1; i >= 0; --i){
        T suma = 0;
        for(int j = i+1; j < n; ++j){
            suma += macierz_U[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/macierz_U[i].at(i);
    }
    return wektor_y;
}


template <typename T, typename K>
vector<T> macierz_przez_wektor(vector<vector<K>> macierz, vector<T> wektor){
    int n = macierz.size();
    vector<T> wynik(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < n; ++j){
            suma += macierz[i].at(j) * wektor[j];
        }
        wynik[i] = suma;
    }
    return wynik;
}

template <typename T>
vector<vector<T>> transponowanie(vector<vector<T>> macierz){
    int n = macierz.size();
    vector<vector<T>> tmp(n, vector<T>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            tmp[i].at(j) = macierz[j].at(i);
        }
    }
    return tmp;
}

int main() {

    ifstream file;

    file.open("macierz.txt");
    string line;
    vector<vector<double>> macierz_A;
    vector<double> wyraz_wolny;
    int n = 0;

    while (getline(file, line)){
        split(line, macierz_A);
        if(n == 0) n = macierz_A[0].size();
    }
    file.close();
    file.open("wyraz_wolny.txt");
    while(getline(file,line)){
        wyraz_wolny.push_back(stod(line));
    }


    vector<vector<double>> macierz_pusta(n, vector<double>(n));
    vector<vector<double>> macierz_jednostkowa(n, vector<double>(n));
    fill_macierz_jednostkowa(macierz_jednostkowa, n);


    vector<vector<double>> macierz_U = macierz_pusta;
    vector<vector<double>> macierz_L = macierz_jednostkowa;
    vector<vector<int>> macierz_permutacji(n, vector<int>(n));

    fill_macierz_jednostkowa(macierz_permutacji, n);

    metoda_doolittla_permutacja(macierz_permutacji, macierz_L, macierz_U, macierz_A);

    cout << "Macierz permutacji: " << endl;
    show_macierz(macierz_permutacji);

    cout << "Macierz L: " << endl;
    show_macierz(macierz_L);

    cout << "Macierz U: " << endl;
    show_macierz(macierz_U);

    auto y = podstawianie_wprzod(macierz_L, wyraz_wolny);
    cout << "Wektor Y:" << endl;
    show_wektor(y);

    auto x_prim = podstawianie_wstecz(macierz_U, y);

    auto p_trans = transponowanie(macierz_permutacji);

    auto x = macierz_przez_wektor(p_trans, x_prim);

    auto LU = mnozenie_macierzy(macierz_L, macierz_U);
    auto LUP = mnozenie_macierzy(LU, macierz_permutacji);
    auto LUPx = macierz_przez_wektor(LUP, x);
    cout << endl;
    show_wektor(LUPx);
    show_wektor(wyraz_wolny);

    return 0;
}