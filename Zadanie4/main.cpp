#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;




template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    int n = macierz.size();
    for(int i = 0; i < n; ++i){
        for(int j = 0 ; j < n; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void fill_macierz(vector<vector<double>> & macierz, int n, vector<double> matrix_values){
    auto it = matrix_values.begin();
    for(int i = 0; i < n; ++i ){
        for (int j = 0; j < n; ++j){
            macierz[i].at(j) = *it;
            it++;
        }
    }
    show_macierz(macierz);
}

template <typename T>
void fill_macierz_jednostkowa(vector<vector<T>> &macierz, int n){
    for(int i = 0; i < n; ++i){
        macierz[i].at(i) = 1;
    }
    //show_macierz(macierz, n);
}

void split_string(string str, vector<double> &out_vector){
    bool sign = false;
    for (char i : str) {
        if(i == ' ') continue;
        if(i == '-'){
            sign = true;
            continue;
        }
        double value = sign ? (-1)*(i - '0') : i - '0';
        if(sign) sign = false;
        out_vector.push_back(value);
    }
}

static bool abs_compare(int a, int b)
{
    return (abs(a) < abs(b));
}

inline void swap_wiersze(vector<vector<double>> &macierz, int x, int y){
    macierz[x].swap(macierz[y]);
}


void swap_kolumny(vector<vector<double>> &macierz, int kolumna1, int kolumna2){
    int size = macierz.size();
    for(int i = 0; i < size; ++i){
        int tmp = macierz[i].at(kolumna2);
        macierz[i].at(kolumna2) = macierz[i].at(kolumna1);
        macierz[i].at(kolumna1) = tmp;
    }

}

int maksymalna_wartosc_bezwzgledna_wiersza(vector<vector<double>> macierz, int n){

    auto max2 = max_element(macierz[n].begin(), macierz[n].end(), abs_compare);
    int index = distance(macierz[n].begin(), max2);
    return index;
}

void metoda_doolittla(vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>> macierz_A){
    int n = macierz_A.size();
    for(int k = 0; k < n; ++k){
        // -- wyznaczanie k-tego wierza macierzy U
        for(int j = k; j < n; ++j){
            double suma = 0;
            for(int p = 0; p < k; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }
        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }
    }
}

void metoda_doolittla_permutacja(vector<vector<int>> &macierz_permutacji, vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>> macierz_A){
    int n = macierz_A.size();
    for(int k = 0; k < n; k++) {
        // -- wyznaczanie k-tego wierza macierzy U
        for (int j = k; j < n; ++j) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }

        int index = maksymalna_wartosc_bezwzgledna_wiersza(macierz_U, k);
        for(int i = 0; i < n; ++i){
            auto tmp = macierz_permutacji[i].at(index);
            macierz_permutacji[i].at(index) = macierz_permutacji[i].at(k);
            macierz_permutacji[i].at(k) = tmp;
        }

        swap_kolumny(macierz_U, k, index);
        swap_kolumny(macierz_A, k, index);

        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }

    }
}

template <typename T, typename K>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> &macierz_L, vector<vector<K>> &macierz_U){
    int n = macierz_L.size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        for(int j = 0; j < n; ++j){
            double suma = 0;
            for(int p = 0; p < n; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    show_macierz(result_macierz);
    return result_macierz;
}


int main() {

    ifstream file;
    /*
     plik matrix w postaci następującej:

     1 2 3
     4 5 6
     7 8 9

     */
    file.open("macierz.txt");
    string line;
    vector<double> matrix_values;
    int n = 0;
    while (getline(file, line)){
        split_string(line, matrix_values);
        if(n == 0) n = matrix_values.size();
    }
    vector<vector<double>> macierz_A(n, vector<double>(n));
    fill_macierz(macierz_A, n, matrix_values);

    vector<vector<double>> macierz_pusta(n, vector<double>(n));
    vector<vector<double>> macierz_jednostkowa(n, vector<double>(n));
    fill_macierz_jednostkowa(macierz_jednostkowa, n);


    vector<vector<double>> macierz_U = macierz_pusta;
    vector<vector<double>> macierz_L = macierz_jednostkowa;
    vector<vector<int>> macierz_permutacji(n, vector<int>(n));


    metoda_doolittla(macierz_L, macierz_U, macierz_A);

    cout << "Macierz Lower: " << endl;
    show_macierz(macierz_L);

    cout << "Macierz Upper: " << endl;
    show_macierz(macierz_U);

    cout << "Mnozenie Macierzy bez permunacji: " << endl;
    mnozenie_macierzy(macierz_L, macierz_U);

    macierz_L = macierz_jednostkowa;
    macierz_U = macierz_pusta;

    fill_macierz_jednostkowa(macierz_permutacji, n);

    metoda_doolittla_permutacja(macierz_permutacji, macierz_L, macierz_U, macierz_A);

    cout << "Macierz permutacji: " << endl;
    show_macierz(macierz_permutacji);

    cout << "Macierz L: " << endl;
    show_macierz(macierz_L);

    cout << "Macierz U: " << endl;
    show_macierz(macierz_U);

    cout << "Macierz UP: " << endl;
    auto mat_up = mnozenie_macierzy(macierz_U, macierz_permutacji);

    cout << "Macierz LUP: " << endl;
    auto mat_lup = mnozenie_macierzy(macierz_L,mat_up);

    cout << "Macierz A: " << endl;
    show_macierz(macierz_A);

    return 0;
}