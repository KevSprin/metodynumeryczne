#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>

using namespace std;


float epsilon_float(int &mantysa){
    float epsilon = 1.0f;
    do{
        epsilon /= 2.0f;
        mantysa++;
    }while ((float) (1.0 + (epsilon / 2.0)) != 1.0);
    return epsilon;
}

double epsilon_double(int &mantysa){
    double epsilon = 1.0;
    do{
        epsilon /= 2.0;
        mantysa++;
    }while ( (1.0 + (epsilon / 2.0)) != 1.0);
    return epsilon;
}

double double_overflow(){
    double x = 1.5;
    do{
        x *= 2.0;
    }while(x * 2.0 < INFINITY);
    return x;
}

float float_overflow(){
    float x = 1.5f;
    do{
        x *= 2.0f;
    }while(x * 2.0f < INFINITY);
    return x;
}

float float_underflow(float epsilon){
    float x = 1.0f + epsilon;
    do{
        x /= 2.0f;
    }while(x / 2.0f > 0);
    return x;
}

double double_underflow(double epsilon){
    double x = 1.0 + epsilon;
    do{
        x /= 2.0;
    }while(x / 2.0 > 0);
    return x;
}

float normalize_float_mantissa( float e_float){
    float exponent = floor(log10(abs(e_float)));
    e_float = float_underflow(e_float) / pow(10.0f, exponent);
    return e_float;
}

double normalize_double_mantissa(double e_double){
    double exponent = floor(log10(abs(e_double)));
    e_double = double_underflow(e_double) / pow(10.0, exponent);
    return e_double;
}

template <typename T>
void zaok_do_najblizszej(T epsilon, int t){
    cout << "Zaokraglanie do nablizszej 1/2 * B^1-t: " << 0.5*pow(2, 1-t) << endl;
}

template <typename T>
void zaok_przez_obcinanie(T epsilon, int t){
    cout << "Zaokraglanie przez obcinanie B^1-t: " << pow(2, 1-t) << endl;
}

int main() {
    float e_float;
    double e_double;
    int e_float_mantysa = 0, e_double_mantysa = 0;


    e_float = epsilon_float(e_float_mantysa);
    e_double = epsilon_double(e_double_mantysa);

    cout << "Epsilon maszynowy dla float = " << e_float << endl;
    zaok_przez_obcinanie(e_float, e_float_mantysa);
    zaok_do_najblizszej(e_float, e_float_mantysa);
    cout << endl;

    cout << "Epsilon maszynowy dla double = " << e_double << endl;
    zaok_przez_obcinanie(e_double, e_double_mantysa);
    zaok_do_najblizszej(e_double, e_double_mantysa);
    cout << endl;


    ofstream float_file("wynik_float_epsilon.txt");
    if(float_file.is_open()){
        float epsilon = 1.0f;
        int i = 1;
        float_file << scientific << setprecision(8) << i << " " << epsilon << " " << (1+epsilon) << endl;
        do{
            i++;
            epsilon /= 2.0f;
            float_file << scientific << setprecision(8) << i << " " << epsilon << " " << (1+epsilon) << endl;
        }while ((float) (1.0 + (epsilon / 2.0)) != 1.0);
    }

    ofstream double_file("wynik_double_epsilon.txt");
    if(double_file.is_open()){
        double epsilon = 1.0;
        int i = 1;
        double_file << scientific << setprecision(8) << i << " " << epsilon << " " << (1+epsilon) << endl;
        do{
            epsilon /= 2.0;
            double_file << scientific << setprecision(8) << i << " " << epsilon << " " << (1+epsilon) << endl;
        }while ( (1.0 + (epsilon / 2.0)) != 1.0);
    }

    int float_cecha_bits = (8* sizeof(float)) - e_float_mantysa - 1;
    int double_cecha_bits = (8* sizeof(double)) - e_double_mantysa - 1;

    cout << "Najmniejsza liczba zmiennopozycyjna float x_min = " << float_underflow(e_float) << endl;
    cout << "Najmniejsza liczba zmiennopozycyjna float znormalizowana x_min_norm = " << normalize_float_mantissa(e_float) << endl;
    cout << "Najwieksza liczba zmiennopozycyjna float x_max = " << float_overflow() << endl;
    cout << "Wielkosc floata w bitach = " << 8*sizeof(float) << endl;
    cout << "Wielkosc mantysy w bitach = " << e_float_mantysa << endl;
    cout << "Wielkosc cechy w bitach = " << float_cecha_bits << endl;
    cout << "Zakres cechy : (" << (-1)*((pow(2,float_cecha_bits - 1 )) - 1) << ", " << pow(2, float_cecha_bits -1) << ")" << endl;
    cout << endl;
    cout << "Najmniejsza liczba zmiennopozycyjna double x_min = " << double_underflow(e_double) << endl;
    cout << "Najmniejsza liczba zmiennopozycyjna double znormalizowana x_min_norm = " << normalize_double_mantissa(e_double) << endl;
    cout << "Najwieksza liczba zmiennopozycyjna double x_max = " << double_overflow() << endl;
    cout << "Wielkosc double w bitach = " << 8*sizeof(double) << endl;
    cout << "Wielkosc mantysy w bitach = " << e_double_mantysa << endl;
    cout << "Wielkosc cechy w bitach = " << double_cecha_bits << endl;
    cout << "Zakres cechy : (" << (-1)*((pow(2,double_cecha_bits - 1 )) - 1) << ", " << pow(2, double_cecha_bits -1) << ")" << endl;

    ofstream wynik("wynik.txt");
    if(wynik.is_open()){
        wynik << "Najmniejsza liczba zmiennopozycyjna float x_min = " << float_underflow(e_float) << endl;
        wynik << "Najmniejsza liczba zmiennopozycyjna float znormalizowana x_min_norm = " << normalize_float_mantissa(e_float) << endl;
        wynik << "Najwieksza liczba zmiennopozycyjna float x_max = " << float_overflow() << endl;
        wynik << "Wielkosc floata w bitach = " << 8*sizeof(float) << endl;
        wynik << "Wielkosc mantysy w bitach = " << e_float_mantysa << endl;
        wynik << "Wielkosc cechy w bitach = " << float_cecha_bits << endl;
        wynik << "Zakres cechy : (" << (-1)*((pow(2,float_cecha_bits - 1 )) - 1) << ", " << pow(2, float_cecha_bits -1) << ")" << endl;
        wynik << endl;
        wynik << "Najmniejsza liczba zmiennopozycyjna double x_min = " << double_underflow(e_double) << endl;
        wynik << "Najmniejsza liczba zmiennopozycyjna double znormalizowana x_min_norm = " << normalize_double_mantissa(e_double) << endl;
        wynik << "Najwieksza liczba zmiennopozycyjna double x_max = " << double_overflow() << endl;
        wynik << "Wielkosc double w bitach = " << 8*sizeof(double) << endl;
        wynik << "Wielkosc mantysy w bitach = " << e_double_mantysa << endl;
        wynik << "Wielkosc cechy w bitach = " << double_cecha_bits << endl;
        wynik << "Zakres cechy : (" << (-1)*((pow(2,double_cecha_bits - 1 )) - 1) << ", " << pow(2, double_cecha_bits -1) << ")" << endl;
    }

    return 0;
}