#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm>
#include <sstream>

using namespace std;

template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    size_t n = macierz.size();
    for(int i = 0; i < n; ++i){
        for(int j = 0 ; j < n; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
    cout << endl;
}

template <typename T>
void show_wektor(const vector<T> wektor){
    size_t n = wektor.size();
    for(int i = 0; i < n; ++i){
        cout << wektor[i] << " ";
    }
    cout << endl;
}

void fill_macierz(vector<vector<double>> & macierz, int n, vector<double> matrix_values){
    auto it = matrix_values.begin();
    for(int i = 0; i < n; ++i ){
        for (int j = 0; j < n; ++j){
            macierz[i].at(j) = *it;
            it++;
        }
    }
    show_macierz(macierz);
}

template <typename T>
void fill_macierz_jednostkowa(vector<vector<T>> &macierz, int n){
    for(int i = 0; i < n; ++i){
        macierz[i].at(i) = 1;
    }
    //show_macierz(macierz, n);
}


void split(string str, vector<vector<double>> &out_vector) {
    vector<double> values;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, ' ')) {
        if(!tok.empty())
            values.push_back(stod(tok));
    }
    out_vector.push_back(values);
}


static bool abs_compare(double a, double b)
{
    return (abs(a) < abs(b));
}

void swap_kolumny(vector<vector<double>> &macierz, int kolumna1, int kolumna2){
    if(kolumna1 == kolumna2) return;
    size_t size = macierz.size();
    for(int i = 0; i < size; ++i){
        int tmp = macierz[i].at(kolumna2);
        macierz[i].at(kolumna2) = macierz[i].at(kolumna1);
        macierz[i].at(kolumna1) = tmp;
    }
}

int maksymalna_wartosc_bezwzgledna_wiersza(vector<vector<double>> macierz, int n){

    auto max2 = max_element(macierz[n].begin(), macierz[n].end(), abs_compare);
    int index = distance(macierz[n].begin(), max2);
    return index;
}

void metoda_doolittla_permutacja(vector<vector<double>> &macierz_permutacji, vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>> macierz_A){
    size_t n = macierz_A.size();
    for(int k = 0; k < n; k++) {
        // -- wyznaczanie k-tego wierza macierzy U
        for (int j = k; j < n; ++j) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }

        int index = maksymalna_wartosc_bezwzgledna_wiersza(macierz_U, k);
        for(int i = 0; i < n; ++i){
            if(k == index) break;
            auto tmp = macierz_permutacji[i].at(index);
            macierz_permutacji[i].at(index) = macierz_permutacji[i].at(k);
            macierz_permutacji[i].at(k) = tmp;
        }

        swap_kolumny(macierz_U, k, index);
        swap_kolumny(macierz_A, k, index);

        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }

    }
}

template <typename T>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> &macierz_L, vector<vector<T>> &macierz_U){
    size_t n = macierz_L.size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        for(int j = 0; j < n; ++j){
            T suma = 0;
            for(int p = 0; p < n; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    return result_macierz;
}

template <typename T>
vector<T> podstawianie_wprzod(vector<vector<T>> macierz_L, vector<T> wektor_b){

    size_t n = macierz_L.size();
    vector<T> wektor_y(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < i; ++j){
            suma += macierz_L[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/ macierz_L[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<T> podstawianie_wstecz(vector<vector<T>> macierz_U, vector<T> wektor_b){

    size_t n = macierz_U.size();
    vector<T> wektor_y(n);
    for(int i = n-1; i >= 0; --i){
        T suma = 0;
        for(int j = i+1; j < n; ++j){
            suma += macierz_U[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/macierz_U[i].at(i);
    }
    return wektor_y;
}


template <typename T>
vector<T> macierz_przez_wektor(vector<vector<T>> macierz, vector<T> wektor){
    size_t n = macierz.size();
    vector<T> wynik(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < n; ++j){
            suma += macierz[i].at(j) * wektor[j];
        }
        wynik[i] = suma;
    }
    return wynik;
}

template <typename T>
vector<vector<T>> transponowanie(vector<vector<T>> macierz){
    size_t n = macierz.size();
    vector<vector<T>> tmp(n, vector<T>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            tmp[i].at(j) = macierz[j].at(i);
        }
    }
    return tmp;
}

template<typename T>
vector<T> get_kolumna(vector<vector<T>> macierz, int kolumna){
    size_t n = macierz.size();
    vector<T> result(n);
    for(int i = 0; i < n; ++i){
        result[i] = macierz[i].at(kolumna);
    }
    return result;
}

template <typename T>
vector<vector<T>> odwracanie(vector<vector<T>> macierz, vector<vector<T>> macierz_jednostkowa, bool upper){
    size_t n = macierz.size();
    vector<vector<T>> macierz_odwrotna(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        auto e = get_kolumna(macierz_jednostkowa, k);

        auto y = upper? podstawianie_wstecz(macierz, e) : podstawianie_wprzod(macierz, e);
        for(int i = 0; i < n; ++i){
            macierz_odwrotna[i].at(k) = y[i];
        }
    }
    return macierz_odwrotna;
}


template <typename T>
T max_element(vector<T> wiersz){
    T tmp = 0;
    for(int i = 0; i < wiersz.size(); ++i){
        T wartosc = abs(wiersz[i]);
        if (wartosc > tmp) tmp = wartosc;
    }
    return tmp;
}

template <typename T>
T iloczyn_skalarny_macierzy(vector<vector<T>> mac1, vector<vector<T>> mac2){

    T suma = 0;
    for(int i = 0; i < mac1.size(); ++i){
        for(int j = 0; j < mac1[0].size(); ++j){
            suma += mac1[i].at(j) * mac2[j].at(i);
        }
    }
    return suma;
}

template <typename T>
T norma_macierzy(vector<vector<T>> macierz){
    vector<T> values;
    for(int j = 0; j < macierz.size(); ++j){
        T suma = 0;
        for(int i = 0; i < macierz[0].size(); ++i){
            suma += abs(macierz[i].at(j));
        }
        values.push_back(suma);
    }
    return max_element(values);
}

template <typename T>
T norma_wektora(vector<T> wektor){
    T suma = 0;
    for(int i = 0; i < wektor.size(); ++i){
        suma += pow(wektor[i], 2);
    }
    return sqrt(suma);
}

template <typename T>
T pierwsza_norma_wektora(vector<T> wektor){
    T suma = 0;
    for(int i = 0; i < wektor.size(); ++i){
        suma += abs(wektor[i]);
    }
    return suma;
}

int main() {

    ifstream file;
    file.open("macierz.txt");
    string line;
    vector<vector<double>> macierz_A;
    vector<double> wyraz_wolny;
    int n = 0;

    while (getline(file, line)){
        split(line, macierz_A);
        if(n == 0) n = macierz_A[0].size();
    }
    file.close();
    file.open("wyraz_wolny.txt");
    while(getline(file,line)){
        wyraz_wolny.push_back(stod(line));
    }


    vector<vector<double>> macierz_pusta(n, vector<double>(n));
    vector<vector<double>> macierz_jednostkowa(n, vector<double>(n));
    fill_macierz_jednostkowa(macierz_jednostkowa, n);

    vector<vector<double>> macierz_U = macierz_pusta;
    vector<vector<double>> macierz_L = macierz_jednostkowa;
    vector<vector<double>> macierz_permutacji(n, vector<double >(n));
    fill_macierz_jednostkowa(macierz_permutacji, n);

    metoda_doolittla_permutacja(macierz_permutacji, macierz_L, macierz_U, macierz_A);


    // ---- niesprzeskalowane
    auto y = podstawianie_wprzod(macierz_L, wyraz_wolny);
    auto x_prim = podstawianie_wstecz(macierz_U, y);
    auto p_trans = transponowanie(macierz_permutacji);
    auto x = macierz_przez_wektor(p_trans, x_prim);


    // ---- skalowanie
    auto macierz_A_skalowana(macierz_A);
    auto wyraz_wolny_skalowany(wyraz_wolny);
    for(int i = 0; i < macierz_A_skalowana.size(); ++i){
        double max_abs = max_element(macierz_A_skalowana[i]);
        for(int j = 0; j < macierz_A_skalowana[i].size(); ++j){
            macierz_A_skalowana[i].at(j) /= max_abs;
        }
        wyraz_wolny_skalowany[i] /= max_abs;
    }

    vector<vector<double>> macierz_L_skalowane(macierz_jednostkowa);
    vector<vector<double>> macierz_U_skalowane(macierz_pusta);
    vector<vector<double>> macierz_permutacji_skalowana(n, vector<double>(n));
    fill_macierz_jednostkowa(macierz_permutacji_skalowana, n);

    metoda_doolittla_permutacja(macierz_permutacji_skalowana, macierz_L_skalowane, macierz_U_skalowane, macierz_A_skalowana);

    auto y_skalowane = podstawianie_wprzod(macierz_L_skalowane, wyraz_wolny_skalowany);
    auto x_prim_skalowane = podstawianie_wstecz(macierz_U_skalowane, y_skalowane);
    auto p_trans_skalowane = transponowanie(macierz_permutacji_skalowana);
    auto x_skalowane = macierz_przez_wektor(p_trans_skalowane, x_prim_skalowane);

    // odwrócone macierze L , U oraz A (skalowane)
    auto macierz_L_skalowana_odwrocona = odwracanie(macierz_L_skalowane, macierz_jednostkowa, false);
    auto macierz_U_skalowana_odwrocona = odwracanie(macierz_U_skalowane, macierz_jednostkowa, true);

    auto macierz_PU_skalowane_odwrocone = mnozenie_macierzy(p_trans_skalowane, macierz_U_skalowana_odwrocona);
    auto macierz_A_skalowana_odwrocona = mnozenie_macierzy(macierz_PU_skalowane_odwrocone, macierz_L_skalowana_odwrocona);

    // odwrócone macierze L , U oraz A
    auto macierz_L_odwrocona = odwracanie(macierz_L, macierz_jednostkowa, false);
    auto macierz_U_odwrocona = odwracanie(macierz_U, macierz_jednostkowa, true);

    auto macierz_PU_odwrocone = mnozenie_macierzy(p_trans, macierz_U_odwrocona);
    auto macierz_A_odwrocona = mnozenie_macierzy(macierz_PU_odwrocone, macierz_L_odwrocona);

    auto ts = norma_macierzy(macierz_A_odwrocona);

    cout << "Macierz A:" << endl;
    show_macierz(macierz_A);
    cout << "Macierz A odwrocona" << endl;
    show_macierz(macierz_A_odwrocona);
    cout << "Macierz A skalowana:" << endl;
    show_macierz(macierz_A_skalowana);
    cout << "Macierz A skalowana odwrocona:" << endl;
    show_macierz(macierz_A_skalowana_odwrocona);
    cout << "A'A'^-1 (skalowane): " << endl;
    show_macierz(mnozenie_macierzy(macierz_A_skalowana, macierz_A_skalowana_odwrocona));
    cout << "AA^-1: " << endl;
    show_macierz(mnozenie_macierzy(macierz_A, macierz_A_odwrocona));

    auto x_norm_definicja = norma_macierzy(macierz_A_odwrocona) * norma_macierzy(macierz_A);
    auto x_norm_szacowane = norma_macierzy(macierz_A) * (pierwsza_norma_wektora(x) / pierwsza_norma_wektora(wyraz_wolny));
    auto x_norm_skalowane_definicja = norma_macierzy(macierz_A_skalowana_odwrocona) * norma_macierzy(macierz_A_skalowana);
    auto x_norm_skalowane_szacowane = (norma_macierzy(macierz_A_skalowana) * (pierwsza_norma_wektora(x_skalowane)) / pierwsza_norma_wektora(wyraz_wolny_skalowany));

    cout << "Wskaznik uwarunkowania macierzy A skalowanej (Z DEFINICJI): " << x_norm_skalowane_definicja << endl;
    cout << "Wskaznik uwarunkowania macierzy A skalowanej (OSZACOWANE): " << x_norm_skalowane_szacowane << endl;
    cout << "Wskaznik uwarunkowania macierzy A (Z DEFINICJI): " << x_norm_definicja << endl;
    cout << "Wskaznik uwarunkowania macierzy A (OSZACOWANE): " << x_norm_szacowane << endl;

    cout << endl;
    // Wypisywanie wyników
    cout << "Wektor x (nieskalowany): " << endl;
    show_wektor(x);
    cout << "Wektor x (skalowany): " << endl;
    show_wektor(x_skalowane);
    cout << endl;
    cout << "Wynik z Ax (nieskalowany): " << endl;
    show_wektor(macierz_przez_wektor(macierz_A, x));
    cout << "Wyraz wolny (nieskalowany): " << endl;
    show_wektor(wyraz_wolny);
    cout << endl;
    cout << "Wynik z Ax (skalowany): " << endl;
    show_wektor(macierz_przez_wektor(macierz_A_skalowana, x_skalowane));
    cout << "Wyraz wolny (skalowany): " << endl;
    show_wektor(wyraz_wolny_skalowany);

    return 0;
}