#include <sstream>
#include <iostream>
#include <cmath>
#include <bitset>
#include <iomanip>
#include <vector>

using namespace std;

int main() {

    float input;
    cout << "Wpisz liczbe zmiennoprzecinkowa: ";
    cin >> input;

    vector<bitset<8>> bitsets;
    unsigned char * p = (unsigned char *)&input;
    for(int i = 0; i != sizeof(input); ++i){
        stringstream ss;
        ss << hex << (int)p[i];
        unsigned n;
        ss >> n;
        bitset<8> b = bitset<8>(n);
        bitsets.push_back(b);
    }
    string mantysa;
    mantysa.insert(0,bitsets.at(0).to_string());
    mantysa.insert(0,bitsets.at(1).to_string());

    string mantysa_substring = bitsets.at(2).to_string().substr(1);
    mantysa.insert(0, mantysa_substring);

    string cecha = bitsets.at(3).to_string().substr(1);
    cecha.append(bitsets.at(2).to_string().substr(0,1));
    string sign = bitsets.at(3).to_string().substr(0,1);

    cout << "Znak: " << sign << endl;
    cout << "Cecha: " << cecha << endl;
    cout << "Mantysa: " << mantysa << endl;
    cout << "Calosc: " << sign << cecha << mantysa << endl;

    float suma_mantysy = 0;
    if (cecha[cecha.length()-1] == '1') suma_mantysy = 1.0;
    for(int i = 0; i < mantysa.length(); ++i){
        int bit = mantysa[i] - '0';
        suma_mantysy += bit / pow(2, i+1);
    }

    int znak = stoi(sign) == 0 ? 1 : -1;

    bitset<8> bity_cechy(cecha);
    int bias = 127;
    string normalizacja = "znormalizowana";
    if(bity_cechy.to_ulong() == 0){
        normalizacja = "zdenormalizowana";
        bias = 126;
    }
    long e = bity_cechy.to_ulong() - bias;

    float wynik = pow(-1,sign[0] - '0') * suma_mantysy * pow(2,e);
    cout << "Reprezentacja znaku w systemie dziesietnym: " << znak << endl;
    cout << "Reprezentacja mantysy w systemie dziesietnym (" << normalizacja << ") : " << suma_mantysy << endl;
    cout << "Reprezentacja cechy w systemie dziesietnym (" << normalizacja << ") : 2^" << e << endl;
    cout << "Wynik powtrotnego przeksztalcenia to: " << wynik << endl;
    return 0;
}