#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

template <typename T>
void sa(T& value, int N){
    for(int n = 1; n <= N; ++n){
        value += (1.0)/((1.0)*n);
    }
}

template <typename T>
void sb(T& value, int N){
    for(int n = N; n > 0; --n){
        value += (1.0)/((1.0)*n);
    }
}


int main() {

    ofstream output_file("wyniki.txt");
    ofstream output_file2("wyniki_operacji.txt");
    if(output_file.is_open() && output_file2.is_open()){
        for(int N = 1000; N <= 3000000; N+=1000){
            double s_dokladne_a = 0.0, s_dokladne_b = 0.0;
            float sa_float = 0.0, sb_float =0.0;
            sa(sa_float, N);
            sb(sb_float, N);
            sa(s_dokladne_a, N);
            sb(s_dokladne_b, N);

            output_file << N << " " << sa_float << " " << s_dokladne_a << " " << sb_float << " " << s_dokladne_b << endl;
            output_file2 << N << " " << (sa_float - s_dokladne_a)/s_dokladne_a << " " << (sb_float - s_dokladne_b)/s_dokladne_b << endl;


        }
    }
    output_file.close();

    return 0;
}