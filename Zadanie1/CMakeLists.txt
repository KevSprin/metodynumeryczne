cmake_minimum_required(VERSION 3.13)
project(mn2test)

set(CMAKE_CXX_STANDARD 14)

add_executable(mn2test main.cpp)