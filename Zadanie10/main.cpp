#include <iostream>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <vector>
#include <limits>
#include <cmath>


using namespace std;

double sign(double value){
    return value > 0 ? 1.0 : -1.0; 
}

template <typename T>
vector<T> wektor_przez_skalar(vector<T> wektor, T skalar){
    int n = wektor.size();
    vector<T> result_wektor(n);
    for(int i = 0; i < n; ++i){
        result_wektor[i] = wektor[i] * skalar;
    }
    return result_wektor;
}

template <typename T>
T norma_euklidesowa(vector<T> wektor){
    T suma = 0;
    for(int i = 0; i < wektor.size(); ++i){
        suma += pow(wektor[i], 2);
    }
    return sqrt(suma);
}

template <typename T>
vector<T> normalizacja_wektora(vector<T> wektor){
    int n = wektor.size();
    vector<T> wektor_znormalizowany(n);
    T norma_wektora_tmp = norma_euklidesowa(wektor);
    for(int i = 0; i < n; ++i){
        wektor_znormalizowany[i] = wektor[i] / norma_wektora_tmp;
    }
    return wektor_znormalizowany;
}

template <typename T>
void insert_kolumna(vector<vector<T>> &macierz, vector<T> kolumna, int index){
    int n = macierz.size();
    for(int i = 0; i < n; ++i){
        macierz[i].at(index) = kolumna[i];
    }
}

template <typename T>
vector<T> get_kolumna(vector<vector<T>> macierz, int index){
    int n = macierz.size();
    vector<T> kolumna(n);
    for(int i = 0; i < n; ++i){
        kolumna[i] = macierz[i].at(index);
    }
    return kolumna;
}

template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    size_t m = macierz.size();
    size_t n = macierz[0].size();
    for(int i = 0; i < m; ++i){
        for(int j = 0 ; j < n; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
}

template <typename T>
void show_wektor(const vector<T> wektor){
    size_t n = wektor.size();
    for(int i = 0; i < n; ++i){
        cout << wektor[i] << " ";
    }
    cout << endl;
}

template <typename T>
vector<vector<T>> transponowanie(vector<vector<T>> macierz){
    int m = macierz.size();
    int n = macierz[0].size();
    vector<vector<T>> tmp(n, vector<T>(m));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < m; ++j){
            tmp[i].at(j) = macierz[j].at(i);
        }
    }
    return tmp;
}

void split(string str, vector<vector<double>> &out_vector) {
    vector<double> values;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, ' ')) {
        if(!tok.empty())
            values.push_back(stod(tok));
    }
    out_vector.push_back(values);
}

vector<vector<double>> Stworz_Macierz_Jednostkowa(int n){
    vector<vector<double>> result(n, vector<double>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            if(i == j)
                result[i].at(j) = 1;
        }
    }
    return result;
}

double Iloczyn_Skalarny_Wektor(vector<double> vector1, vector<double> vector2){
    double result = 0;
    int n = vector1.size();
    for(int i= 0; i < n; ++i){
        result += vector1[i] * vector2[i];
    }
    return result;
}

vector<vector<double>> Macierz_Odjac_Skalar(vector<vector<double>> macierz, double skalar){
    int m = macierz.size();
    int n = macierz[0].size();

    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            macierz[i].at(j) -= skalar;
        }
    }
    return macierz;
}

template <typename T>
vector<T> macierz_przez_wektor(vector<vector<T>> macierz, vector<T> wektor){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    vector<T> wynik(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < m; ++j){
            suma += macierz[i].at(j) * wektor[j];
        }
        wynik[i] = suma;
    }
    return wynik;
}

vector<vector<double>> Macierz_Przez_Skalar(vector<vector<double>> macierz, double skalar){
    int m = macierz.size();
    int n = macierz[0].size();

    for(int i = 0 ; i < m; ++i){
        for(int j = 0; j < n; ++j){
            macierz[i].at(j) *= skalar; 
        }
    }
    return macierz;
}

vector<vector<double>> Macierz_Odjac_Macierz(vector<vector<double>> macierz1, vector<vector<double>> macierz2){
    if(macierz1.size() != macierz2.size() && macierz1[0].size() != macierz2[0].size()){
        throw invalid_argument("Number of cols of mat_1 and number of rows of mat_2 are not equal");
    }
    int m = macierz1.size();
    int n = macierz1[0].size();
    vector<vector<double>> result(m, vector<double>(n));

    for(int i = 0; i < m; ++i){
        for(int j = 0 ; j < n; ++j){
            result[i].at(j) = macierz1[i].at(j) - macierz2[i].at(j);
        }
    }
    return result;
}

template <typename T>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> macierz_1, vector<vector<T>> macierz_2){
    size_t m = macierz_1.size();
    size_t n = macierz_2[0].size();
    size_t collective_n = macierz_1[0].size();
    if(macierz_1[0].size() != macierz_2.size()) throw invalid_argument("Number of cols of mat_1 and number of rows of mat_2 are not equal");
    vector<vector<T>> result_macierz(m, vector<T>(n));
    for(int k = 0; k < m; ++k){
        for(int j = 0; j < n; ++j){
            T suma = 0;
            for(int p = 0; p < collective_n; ++p){
                suma += macierz_1[k].at(p) * macierz_2[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    return result_macierz;
}

static bool abs_compare(double a, double b)
{
    return (fabs(a) < fabs(b));
}

int maksymalna_wartosc_bezwzgledna_wiersza(vector<vector<double>> macierz, int n){

    auto max2 = max_element(macierz[n].begin(), macierz[n].end(), abs_compare);
    int index = distance(macierz[n].begin(), max2);
    return index;
}

void swap_kolumny(vector<vector<double>> &macierz, int kolumna1, int kolumna2){

    size_t size = macierz.size();
    for(int i = 0; i < size; ++i){
        double tmp = macierz[i].at(kolumna2);
        macierz[i].at(kolumna2) = macierz[i].at(kolumna1);
        macierz[i].at(kolumna1) = tmp;
    }
}

void swap_row(vector<vector<double>> &macierz, int wiersz1, int wiersz2){

    auto wiersz_tmp = macierz[wiersz1];
    macierz[wiersz1] = macierz[wiersz2];
    macierz[wiersz2] = wiersz_tmp;
}

void metoda_doolittla_permutacja(vector<vector<double>> &macierz_permutacji, vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>>  &macierz_A){
    int n = macierz_A.size();
    for(int k = 0; k < n; k++) {
        // -- wyznaczanie k-tego wierza macierzy U
        for (int j = k; j < n; ++j) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }

        int index = maksymalna_wartosc_bezwzgledna_wiersza(macierz_U, k);
        if(k != index) {
            swap_row(macierz_permutacji, k, index);
            swap_kolumny(macierz_U, k, index);
            swap_kolumny(macierz_A, k, index);
        }

        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }

    }
}

vector<double> Obliczenie_y_k(vector<vector<double>> macierz, double t_k, int k){
    int n = macierz[0].size();
    vector<double> y(n);
    if(t_k == 0) return y;
    for(int j = 0; j < n; ++j){
        if(j <= k){
            y[j] = 0;
        }
        else if(j == k + 1){
            y[j] = sqrt(0.5*(1 + (fabs(macierz[k].at(k+1))/ t_k)));    
        }
        else{
            double d_k = sign(macierz[k].at(k+1))/ (2*t_k*y[k+1]);
            y[j] = d_k * macierz[k].at(j);
        }
    }
    return y;
}

vector<double> Obliczenie_x_k(vector<vector<double>> macierz, double s_k, int k){
    int m = macierz.size();
    vector<double> x(m);
    if(s_k == 0) return x;
    for(int i = 0; i < m; ++i){
        if(i < k){
            x[i] = 0;
        }
        else if(i == k){
            x[i] = sqrt( 0.5*(1 + (fabs(macierz[k].at(k))/s_k)));
        }
        else{
            double c_k = sign(macierz[k].at(k))/ (2 * s_k * x[k]);
            x[i] = c_k * macierz[i].at(k);
        }
    }
    
    return x;
}

double Obliczenie_t_k(vector<vector<double>> macierz, int k){
    int n = macierz[0].size();
    double result = 0;
    for(int j = k+1; j < n; ++j){
        result += pow(fabs(macierz[k].at(j)), 2);
    }
    return sqrt(result);
}

double Obliczenie_s_k(vector<vector<double>> macierz, int k){
    int m = macierz.size();
    double result = 0;
    for(int i = k; i < m; ++i){
        result += pow(fabs(macierz[i].at(k)),2);
    }
    return sqrt(result);
}

vector<vector<double>> Wektor_Przez_Wektor_T(vector<double> wektor, vector<double> wektor_T){
    int n = wektor.size();
    vector<vector<double>> result(n, vector<double>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            result[i].at(j) = wektor[i]*wektor_T[j];
        }
    }
    return result;
}

vector<vector<double>> Obliczenie_Macierzy_P(vector<double> x){

    int m = x.size();   
    auto x_macierz = Macierz_Przez_Skalar(Wektor_Przez_Wektor_T(x,x), 2.0);
    vector<vector<double>> macierz_jednostkowa = Stworz_Macierz_Jednostkowa(m);
    vector<vector<double>> macierz_P = Macierz_Odjac_Macierz(macierz_jednostkowa, x_macierz);
    return macierz_P;
}

vector<vector<double>> Obliczanie_Macierzy_Q(vector<double> y){
    int n = y.size();
    auto y_macierz = Macierz_Przez_Skalar(Wektor_Przez_Wektor_T(y,y), 2.0);
    vector<vector<double>> macierz_jednostkowa = Stworz_Macierz_Jednostkowa(n);
    vector<vector<double>> macierz_Q = Macierz_Odjac_Macierz(macierz_jednostkowa, y_macierz);
    return macierz_Q;
}

vector<vector<double>> Refresh_Macierz(vector<vector<double>> macierz){
    int m = macierz.size();
    int n = macierz[0].size();

    vector<vector<double>> result(m, vector<double>(n));
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            if(fabs(macierz[i].at(j)) < 1e-14) result[i].at(j) = 0;
            else result[i].at(j) = macierz[i].at(j);
        }
    }
    return result;
}

vector<double> Refresh_Wektor(vector<double> wektor){
    int n = wektor.size();
    vector<double> result(n);
    for(int i = 0; i < n; ++i){
        if(fabs(wektor[i]) < 1e-14) result[i] = 0;
        else result[i] = wektor[i];
    }
    return result;
}

vector<double> Wartosci_Diagonalne(vector<vector<double>> macierz){
    int n = macierz[0].size();
    vector<double> result(n);
    for(int i = 0; i < n; ++i){
        result[i] = macierz[i].at(i); 
    }
    result = Refresh_Wektor(result);
    return result;
}

vector<double> Wartosci_Pozadiagonalne(vector<vector<double>> macierz){
    int n = macierz[0].size();
    vector<double> result(n);
    for(int i = 1; i < n; ++i){
        result[i] = macierz[i].at(i-1);
    }
    result = Refresh_Wektor(result);
    return result;
}

void lambda_min_max(vector<double> wartosci_diagonalne, vector<double> wartosci_pozadiagonalne, double &min, double &max){

    int n = wartosci_diagonalne.size();
    wartosci_pozadiagonalne.push_back(0);
    double tmp_min=NAN, tmp_max=NAN;
    for(int i = 0; i < n; ++i){
        double min_value = wartosci_diagonalne[i] - (fabs(wartosci_pozadiagonalne[i]) + fabs(wartosci_pozadiagonalne[i+1]));
        double max_value = wartosci_diagonalne[i] + (fabs(wartosci_pozadiagonalne[i]) + fabs(wartosci_pozadiagonalne[i+1]));
        if (isnan(tmp_min)) tmp_min = min_value;
        else if(tmp_min > min_value) tmp_min = min_value;

        if (isnan(tmp_max)) tmp_max = max_value;
        else if(tmp_max < max_value) tmp_max = max_value;
    }
    min = tmp_min;
    max = tmp_max;
}

vector<vector<double>> Macierz_P(vector<vector<vector<double>>> macierze){
    vector<vector<double>> tmp = macierze.front();
    for(int i = 1; i < macierze.size(); ++i){
        tmp = mnozenie_macierzy(tmp, macierze[i]);
    }
    tmp = Refresh_Macierz(tmp);
    return tmp;
}

vector<vector<double>> Macierz_Q(vector<vector<vector<double>>> macierze){
    vector<vector<double>> tmp = macierze.back();
    for(int i = macierze.size()-2; i >= 0; --i){
        tmp = mnozenie_macierzy(tmp, macierze[i]);
    }
    tmp = Refresh_Macierz(tmp);
    return tmp;
}

vector<vector<double>> Popraw_Macierz_J(vector<vector<double>> macierz){
    int m = macierz.size();
    int n = macierz[0].size();
    vector<vector<double>> result(m, vector<double>(n));
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            if(i == j || i+1 == j)
                result[i].at(j) = macierz[i].at(j);
        }
    }
    return result;
}

void Obliczenie_Macierzy_J(vector<vector<double>> macierz_A, vector<vector<double>> &macierz_P, vector<vector<double>> &macierz_Q, vector<vector<double>> &macierz_J){
    
    int n = macierz_A[0].size();
    int m = macierz_A.size();
    vector<vector<vector<double>>> macierze_P;
    vector<vector<vector<double>>> macierze_Q;
    for(int k = 0; k < n; ++k){

        // Wyznaczanie wektora jednostkowego x
        double s_k = Obliczenie_s_k(macierz_A, k);
        double alfa_k = -s_k * sign(macierz_A[k].at(k));      
        vector<double> x = Obliczenie_x_k(macierz_A, s_k, k);

        // Wynzaczanie macierzy ortogonalnej P
        macierz_P = Obliczenie_Macierzy_P(x);

        macierze_P.push_back(macierz_P);

        // Wyznaczanie macierzy A(k+1/2)
        vector<vector<double>> macierz_A_tmp = mnozenie_macierzy(macierz_P, macierz_A);

        if(k == n-1){
            macierz_J = Refresh_Macierz(macierz_A_tmp);
            continue;
        }       
        // Wyznaczanie wektora jednostkowego y
        double t_k = Obliczenie_t_k(macierz_A_tmp, k);
        double beta_k = -t_k * sign(macierz_A_tmp[k].at(k+1));
        vector<double> y = Obliczenie_y_k(macierz_A_tmp, t_k, k);

        // Wyznaczanie macierzy ortogonalnej Q
            macierz_Q = Refresh_Macierz(Obliczanie_Macierzy_Q(y));

        macierze_Q.push_back(macierz_Q);

        // Wynzaczanie macierzy A(k+1)
        vector<vector<double>> macierz_A_k1 = mnozenie_macierzy(macierz_A_tmp, macierz_Q);
        
        macierz_A = Refresh_Macierz(macierz_A_k1);
    }
    macierz_P = Macierz_P(macierze_P);
    macierz_Q = Macierz_Q(macierze_Q);
    macierz_J = Popraw_Macierz_J(macierz_J);
}

int Twierdzenie_Sturma(double value, vector<double> diag, vector<double> poza_diag){

    int n = diag.size();
    vector<double> q(n);
    int ujemne_wartosci = 0;
    for(int i = 0; i < n; ++i){
        if(i == 0){
            q[i] = diag[i] - value;
        }
        else{
            if(q[i-1] != 0) // zabezpieczeniem przed dzieleniem przez zero
                q[i] = (diag[i] - value) - (poza_diag[i]*poza_diag[i]) / q[i-1];
            else
                q[i] = (diag[i] - value) - fabs(poza_diag[i]) / numeric_limits<double>().epsilon();
        }
        if(q[i] < 0) ujemne_wartosci++;
    }
    return ujemne_wartosci;
} 

vector<double> Metoda_Bisekcji(vector<double> wartosci_diagonalne, vector<double> wartosci_pozadiagonalne){
    int n = wartosci_diagonalne.size();
    vector<double> dolne_ograniczenia(n);
    vector<double> gorne_ograniczenia(n);

    double lambda_min = 0, lambda_max = 0;
    lambda_min_max(wartosci_diagonalne, wartosci_pozadiagonalne, lambda_min, lambda_max);

    for(int i = 0; i < n; ++i){
        dolne_ograniczenia[i] = lambda_min;
        gorne_ograniczenia[i] = lambda_max;
    }

    for(int k = n-1; k >=0; --k){
        double upper = gorne_ograniczenia[k];
        double lower = dolne_ograniczenia[k];
        double value = 0;
        while(fabs(upper - lower) > 1e-15){
            if(value == upper - lower) break;
            value = upper - lower;
            gorne_ograniczenia[k] = (upper + lower)/2.0;
            int a = Twierdzenie_Sturma(gorne_ograniczenia[k], wartosci_diagonalne, wartosci_pozadiagonalne);
            if(a <= k){
                lower = gorne_ograniczenia[k];
            }else{
                upper = gorne_ograniczenia[k];
            }
            for(int i = 0; i < a; ++i){
                if(gorne_ograniczenia[i] > gorne_ograniczenia[k]){
                    gorne_ograniczenia[i] = gorne_ograniczenia[k];
                }
            }
            for(int i = a; i < k; ++i){
                if(dolne_ograniczenia[i] < gorne_ograniczenia[k]){
                    dolne_ograniczenia[i] = gorne_ograniczenia[k];
                }
            }
        }
    }
    return gorne_ograniczenia;
}

template <typename T>
vector<T> podstawianie_wstecz_zadanie9(vector<vector<T>> macierz_U){

    size_t n = macierz_U.size();
    vector<T> wektor_y(n);
    wektor_y[n-1] = 1;
    for(int i = n-2; i >= 0; --i){
        T suma = 0.0;
        for(int j = i+1; j < n; ++j){
            suma += macierz_U[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (0.0 - suma)/macierz_U[i].at(i);
    }
    return wektor_y;
}

vector<vector<double>> Obliczenie_Y(vector<vector<double>> macierz_K, vector<double> wartosci_wlasne){

    int n = macierz_K.size();
    auto macierz_jednostkowa = Stworz_Macierz_Jednostkowa(n);
    vector<vector<double>> macierz_Y(n, vector<double>(n));

    for(int i = 0; i < n; ++i){
        auto tmp_macierz = Macierz_Przez_Skalar(macierz_jednostkowa, wartosci_wlasne[i]);
        auto macierz_A = Macierz_Odjac_Macierz(macierz_K, tmp_macierz);
        vector<vector<double>> macierz_L = Stworz_Macierz_Jednostkowa(n);
        vector<vector<double>> macierz_U(n, vector<double>(n));
        vector<vector<double>> macierz_P = Stworz_Macierz_Jednostkowa(n);

        metoda_doolittla_permutacja(macierz_P, macierz_L, macierz_U, macierz_A);
        auto x_prim = podstawianie_wstecz_zadanie9(macierz_U);
        auto macierz_P_trans = transponowanie(macierz_P);
        auto wektor_wlasny_i = macierz_przez_wektor(macierz_P_trans, x_prim);
        auto norm_wektor = normalizacja_wektora(wektor_wlasny_i);
        insert_kolumna(macierz_Y, norm_wektor, i);
    }
    macierz_Y = Refresh_Macierz(macierz_Y);
    return macierz_Y;
}

/// TODO
vector<vector<double>> Obliczenie_X(vector<vector<double>> macierz_J, vector<vector<double>> macierz_Y, vector<double> wartosci_szczegolne){

    int n = macierz_J.size();
    vector<vector<double>> macierz_X(n, vector<double>(n));
    for(int i = 0; i < n; ++i){
        if(i == n-1 || wartosci_szczegolne[i] == 0) continue;
        auto y_i = get_kolumna(macierz_Y, i);
        auto x_i = macierz_przez_wektor(macierz_J, y_i);

        double mnoznik = 1/wartosci_szczegolne[i];        
        auto tmp_xi_sigma = wektor_przez_skalar(x_i, mnoznik);
        insert_kolumna(macierz_X, tmp_xi_sigma, i);
    }
    macierz_X = Refresh_Macierz(macierz_X);
    return macierz_X;
}

vector<double> Obliczenie_Wartosci_Szczegolnych(vector<double> wartosci_wlasne){
    int n = wartosci_wlasne.size();
    vector<double> result(n); 
    wartosci_wlasne = Refresh_Wektor(wartosci_wlasne);
    for(int i = 0; i < n ; ++i){
        result[i] = sqrt(wartosci_wlasne[n-(i+1)]);
    }
    result = Refresh_Wektor(result);
    return result;
}

vector<vector<double>> Obliczenie_Macierzy_Sigma(vector<double> wartosci_szczegolne, int m, int n){

    vector<vector<double>> result(m, vector<double>(n));
    for(int i = 0; i < n; ++i){
        result[i].at(i) = wartosci_szczegolne[i];
    }
    result = Refresh_Macierz(result);
    return result;
}

vector<vector<double>> Obliczenie_Pseudoodwrotnej_Sigma(vector<vector<double>> macierz, vector<double> wartosci_szczegolne){
    int n = macierz.size();
    for(int i = 0; i < n; ++i){
        if(fabs(wartosci_szczegolne[i]) < 1e-15) macierz[i].at(i) = 0;
        macierz[i].at(i) = 1/wartosci_szczegolne[i];
        if(macierz[i].at(i) == INFINITY) macierz[i].at(i) = 0;
    }
    return macierz;
}

int main(){

    int m = 0;
    int n = 0;
    ifstream file;
    // można wybrać spośród macierz 1, 2 oraz 3
    file.open("macierz3.txt");
    string line;
    vector<vector<double>> macierz_A;
    vector<vector<double>> macierz_J;
    vector<vector<double>> macierz_P;
    vector<vector<double>> macierz_Q;
    while (getline(file, line)){
        split(line, macierz_A);
        if(n == 0) n = macierz_A[0].size();
    }
    m = macierz_A.size();

    Obliczenie_Macierzy_J(macierz_A, macierz_P, macierz_Q, macierz_J);

    auto macierz_Q_trans = macierz_Q;
    macierz_Q = transponowanie(macierz_Q);
    
    // Sprawdzenie PJQ
    auto tmp_P_J = mnozenie_macierzy(macierz_P, macierz_J);
    auto tmp_A = mnozenie_macierzy(tmp_P_J, macierz_Q_trans);

    // Wyswietlenie macierzy PJQ
    cout << "Macierz P:" << endl;
    show_macierz(macierz_P); cout << endl;
    cout << "Macierz Q trans: " << endl;
    show_macierz(macierz_Q_trans); cout << endl;
    cout << "Macierz J: " << endl;
    show_macierz(macierz_J); cout << endl;
    cout << "Macierz J (transponowane): " << endl;
    show_macierz(transponowanie(macierz_J)); cout << endl;
    cout << "Wynik PJQ': " << endl;
    show_macierz(tmp_A); cout << endl;

    // Nastepnie rozklad macierzy dwudiagonalnej J
    cout << "Macierz K: " << endl;
    vector<vector<double>> macierz_K = mnozenie_macierzy(transponowanie(macierz_J),macierz_J);
    show_macierz(macierz_K); cout << endl;

    // Pozyskanie wartosci diagonalnych i wartosci pozadiagonalnych
    vector<double> wartosci_diag = Wartosci_Diagonalne(macierz_K);
    vector<double> wartosci_poza_diag = Wartosci_Pozadiagonalne(macierz_K);
    cout << "Wartosci diagonalne: ";
    show_wektor(wartosci_diag); cout << endl;
    cout << "Wartosci pozadiagonalne: ";
    show_wektor(wartosci_poza_diag); cout << endl;

    // Obliczenie Macierzy X, Y, Sigma
    // Metoda bisekcji -> pozyskanie wartości własnych macierzy za pomocą wartości diagonalnych i pozadiagonalnych
    vector<double> wartosci_wlasne_K = Metoda_Bisekcji(wartosci_diag, wartosci_poza_diag);

    // Pozyskanie wartości szczególnych macierzy z wartości własnych macierzy (pierwiastki)
    vector<double> wartosci_szczegolne = Obliczenie_Wartosci_Szczegolnych(wartosci_wlasne_K);

    reverse(wartosci_wlasne_K.begin(), wartosci_wlasne_K.end());
    // Obliczenie Macierzy Y kolumnami (wstawienia wstecz) z wartościami własnymi
    vector<vector<double>> macierz_Y = Obliczenie_Y(macierz_K, wartosci_wlasne_K);
    vector<vector<double>> macierz_Y_trans = transponowanie(macierz_Y);

    // Obliczenie macierzy X za pomocą macierzy J , macierzy Y oraz wartości szczególnych
    vector<vector<double>> macierz_X = Obliczenie_X(macierz_J, macierz_Y, wartosci_szczegolne);

    // Składanie macierzy Sigma (wartości szczególne na przekątnej w nierosnącej kolejności)
    vector<vector<double>> macierz_Sigma = Obliczenie_Macierzy_Sigma(wartosci_szczegolne, macierz_X.size(), macierz_Y.size());
  
    cout << "Wartosci wlasne macierzy K: "<< endl;
    show_wektor(wartosci_wlasne_K); cout << endl;

    cout << "Wartosci szczegolne: " << endl;
    show_wektor(wartosci_szczegolne); cout << endl;

    cout << "Macierz X: " << endl;
    show_macierz(macierz_X); cout << endl;

    cout << "Macierz Y: " << endl;
    show_macierz(macierz_Y); cout << endl;

    cout << "Macierz Sigma: " << endl;
    show_macierz(macierz_Sigma); cout << endl;

    // Obliczenie Macierzy U i V
    // P * X
    vector<vector<double>> macierz_U = mnozenie_macierzy(macierz_P, macierz_X);

    // Q * Y
    vector<vector<double>> macierz_V = mnozenie_macierzy(macierz_Q, macierz_Y);

    cout << "Macierz U: " << endl;
    show_macierz(macierz_U); cout << endl;

    cout << "Macierz V: " << endl;
    show_macierz(macierz_V); cout << endl;

    // U * SIGMA
    auto macierz_U_Sigma = mnozenie_macierzy(macierz_U, macierz_Sigma);

    // U_SIGMA * V'
    auto A = mnozenie_macierzy(macierz_U_Sigma, transponowanie(macierz_V));
    cout << "Macierz A z U * SIGMA * V': " << endl;
    show_macierz(A); cout << endl;

    cout << "Macierz Pseudoodwrotna Sigma: " << endl;
    auto macierz_Sigma_Pseudoodwrotna = Obliczenie_Pseudoodwrotnej_Sigma(transponowanie(macierz_Sigma), wartosci_szczegolne);
    show_macierz(macierz_Sigma_Pseudoodwrotna); cout << endl;

    auto macierz_U_trans = transponowanie(macierz_U);
    auto macierz_V_Sigma_Pseudo = mnozenie_macierzy(macierz_V, macierz_Sigma_Pseudoodwrotna);
    auto macierz_A_Pseudo = mnozenie_macierzy(macierz_V_Sigma_Pseudo, macierz_U_trans);

    cout << "Macierz Pseudoodwrotna A: " << endl;
    show_macierz(macierz_A_Pseudo); cout << endl;

    auto macierz_A_PSEUDO_A = mnozenie_macierzy(macierz_A_Pseudo, macierz_A);
    cout << "A_PSEUDO * A: "  << endl;
    show_macierz(macierz_A_PSEUDO_A); cout << endl;

    auto macierz_A_A_PSEUDO = mnozenie_macierzy(macierz_A, macierz_A_Pseudo);
    cout << "A * A_PSEUDO: " << endl;
    show_macierz(macierz_A_A_PSEUDO); cout << endl;

    auto macierz_Sigma_PSEUDO_SIGMA = mnozenie_macierzy(macierz_Sigma_Pseudoodwrotna,macierz_Sigma);
    cout << "SIGMA_PSEUDO * SIGMA: " << endl;
    show_macierz(macierz_Sigma_PSEUDO_SIGMA); cout << endl;

    auto macierz_Sigma_Sigma_PSEUDO = mnozenie_macierzy(macierz_Sigma, macierz_Sigma_Pseudoodwrotna);
    cout << "SiGMA * SIGMA_PSEUDO: " << endl;
    show_macierz(macierz_Sigma_Sigma_PSEUDO);

    return 0;
}