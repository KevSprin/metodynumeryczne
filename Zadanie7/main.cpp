#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <cmath>

#define SZUM 0

using namespace std;

template <typename T>
T max_element(vector<T> wiersz){
    T tmp = 0;
    for(int i = 0; i < wiersz.size(); ++i){
        T wartosc = abs(wiersz[i]);
        if (wartosc > tmp) tmp = wartosc;
    }
    return tmp;
}

template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    for(int i = 0; i < n; ++i){
        for(int j = 0 ; j < n; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
    cout << endl;
}

template <typename T>
void show_wektor(const vector<T> wektor){
    size_t n = wektor.size();
    for(int i = 0; i < n; ++i){
        cout << wektor[i] << " ";
    }
    cout << endl;
}

template <typename T>
void fill_macierz_jednostkowa(vector<vector<T>> &macierz, int n){
    for(int i = 0; i < n; ++i){
        macierz[i].at(i) = 1;
    }
}

static bool abs_compare(double a, double b)
{
    return (abs(a) < abs(b));
}

void swap_kolumny(vector<vector<double>> &macierz, int kolumna1, int kolumna2){

    size_t size = macierz.size();
    for(int i = 0; i < size; ++i){
        double tmp = macierz[i].at(kolumna2);
        macierz[i].at(kolumna2) = macierz[i].at(kolumna1);
        macierz[i].at(kolumna1) = tmp;
    }
}

void swap_row(vector<vector<double>> &macierz, int wiersz1, int wiersz2){

    auto wiersz_tmp = macierz[wiersz1];
    macierz[wiersz1] = macierz[wiersz2];
    macierz[wiersz2] = wiersz_tmp;
}

int maksymalna_wartosc_bezwzgledna_wiersza(vector<vector<double>> macierz, int n){

    auto max2 = max_element(macierz[n].begin(), macierz[n].end(), abs_compare);
    int index = distance(macierz[n].begin(), max2);
    return index;
}

void metoda_doolittla_permutacja(vector<vector<double>> &macierz_permutacji, vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>>  &macierz_A){
    int n = macierz_A.size();
    for(int k = 0; k < n; k++) {
        // -- wyznaczanie k-tego wierza macierzy U
        for (int j = k; j < n; ++j) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }

        int index = maksymalna_wartosc_bezwzgledna_wiersza(macierz_U, k);
        if(k != index) {
            swap_row(macierz_permutacji, k, index);
            swap_kolumny(macierz_U, k, index);
            swap_kolumny(macierz_A, k, index);
        }

        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }

    }
}

vector<double> Generowanie_x_z_zakresem(int zakres, int n){
    vector<double> x;
    int start = (-1)*zakres, stop = zakres;
    int odleglosc = abs(start - stop);
    int krok = odleglosc / n;
    for(int i = start; i < stop ; i+=krok){
        x.push_back(i);
    }
    return x;
}

vector<double> Generowanie_x(int n){
    vector<double> x(n);
    for(int i = 0; i < n; ++i){
        x[i] = (double)i+1;
    }
    return x;
}

vector<double> Generowanie_sigma_standart(int n){
    vector<double> sigma(n);
    for(int i = 0; i < n; ++i){
        sigma[i] = 1;
    }
    return sigma;
}

vector<double> Pobieranie_szumu(int n){
    vector<double> szum;
    string line;
    ifstream file;
#if SZUM
    file.open("szum.txt");
#else
    file.open("szum2.txt");
#endif
    int counter = 0;
    while(getline(file,line)){
        if(counter >= n) break;
        szum.push_back(stod(line));
        counter++;
    }
    return szum;
}

template <typename T>
vector<T> macierz_przez_wektor(vector<vector<T>> macierz, vector<T> wektor){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    vector<T> wynik(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < m; ++j){
            suma += macierz[i].at(j) * wektor[j];
        }
        wynik[i] = suma;
    }
    return wynik;
}

template<typename T>
vector<T> get_kolumna(vector<vector<T>> macierz, int kolumna){
    size_t n = macierz.size();
    vector<T> result(n);
    for(int i = 0; i < n; ++i){
        result[i] = macierz[i].at(kolumna);
    }
    return result;
}

template <typename T>
vector<T> podstawianie_wprzod(vector<vector<T>> macierz_L, vector<T> wektor_b){

    size_t n = macierz_L.size();
    vector<T> wektor_y(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < i; ++j){
            suma += macierz_L[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/ macierz_L[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<T> podstawianie_wstecz(vector<vector<T>> macierz_U, vector<T> wektor_b){

    size_t n = macierz_U.size();
    vector<T> wektor_y(n);
    for(int i = n-1; i >= 0; --i){
        T suma = 0;
        for(int j = i+1; j < n; ++j){
            suma += macierz_U[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/macierz_U[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<vector<T>> odwracanie(vector<vector<T>> macierz, vector<vector<T>> macierz_jednostkowa, bool upper){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    vector<vector<T>> macierz_odwrotna(n, vector<T>(m));
    for(int k = 0; k < n; ++k){
        auto e = get_kolumna(macierz_jednostkowa, k);
        auto y = upper? podstawianie_wstecz(macierz, e) : podstawianie_wprzod(macierz, e);
        for(int i = 0; i < m; ++i){
            macierz_odwrotna[i].at(k) = y[i];
        }
    }
    return macierz_odwrotna;
}

template <typename T>
vector<vector<T>> transponowanie(vector<vector<T>> macierz){
    int n = macierz.size();
    int m = macierz[0].size();
    vector<vector<T>> tmp(m, vector<T>(n));
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            tmp[i].at(j) = macierz[j].at(i);
        }
    }
    return tmp;
}

template <typename T>
T norma_macierzy(vector<vector<T>> macierz){
    vector<T> values;
    for(int j = 0; j < macierz.size(); ++j){
        T suma = 0;
        for(int i = 0; i < macierz[0].size(); ++i){
            suma += abs(macierz[i].at(j));
        }
        values.push_back(suma);
    }
    return max_element(values);
}

// x^3 + 2x^2 + 3^x + 4 = f(x)
double funkcja_ustalona(double x){
    return 4 + 3*x + 2*pow(x,2) + pow(x,3);
}

double f_k(int k, double x){
    return pow(x, k-1);
}

vector<double> Obliczenie_y(vector<double> x, vector<double> sigma){
    int n = x.size();
    vector<double> y(n);
    for(int i = 0; i < n; ++i){
#if SZUM
        y[i] = funkcja_ustalona(x[i]) + sigma[i];
#else
        y[i] = funkcja_ustalona(x[i]);
#endif
    }
    return y;
}

vector<vector<double>> Konstrukcja_Macierzy_A(vector<double> x, vector<double> sigma, int m){

    int n = x.size();
    vector<vector<double>> macierz_A(n, vector<double>(m));
    for(int i = 0; i < n; ++i){

        for(int j = 0; j < m; ++j){
            macierz_A[i].at(j) = f_k(j+1, x[i]) / sigma[i];
        }
    }
    return macierz_A;
}

vector<double> Konstrukcja_Wektora_b(vector<double> y, vector<double> sigma){
    int n = y.size();
    vector<double> b(n);
    for(int i = 0; i < n; ++i){
        b[i] = y[i] / sigma[i];
    }
    return b;
}

template <typename T>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> macierz_L, vector<vector<T>> macierz_U){
    size_t n = macierz_L.size();
    size_t m = macierz_L[0].size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        for(int j = 0; j < n; ++j){
            T suma = 0;
            for(int p = 0; p < m; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    return result_macierz;
}

template <typename T>
void zwroc_wartosci_wariancji_kowariancji(vector<vector<T>> macierz){
    cout << "Wartosci wariancji (na przekatnej): ";
    for(int i = 0; i < macierz.size(); ++i){
        cout << macierz[i].at(i) << " ";
    }
    cout << endl;
    cout << "Wartosci kowariancji (pozaprzekatnej): ";
    for(int i = 0; i < macierz.size(); ++i){
        for(int j = 0; j < macierz[0].size(); ++j){
            if (i == j) continue;
            cout << macierz[i].at(j) << " ";
        }
    }
    cout << endl;
}

int main() {

    int n = 10;
    int m = 4;
    int zakres = 5;

    vector<double> x = Generowanie_x_z_zakresem(zakres, n);
    vector<double> sigma = Pobieranie_szumu(n);
    vector<double> y = Obliczenie_y(x, sigma);

    vector<vector<double>> macierz_pusta(m, vector<double>(m));
    vector<vector<double>> macierz_jednostkowa(m, vector<double>(m));
    fill_macierz_jednostkowa(macierz_jednostkowa, m);

    vector<vector<double>> macierz_U = macierz_pusta;
    vector<vector<double>> macierz_L = macierz_jednostkowa;
    vector<vector<double>> macierz_permutacji(m, vector<double>(m));
    fill_macierz_jednostkowa(macierz_permutacji, m);

    auto macierz_A = Konstrukcja_Macierzy_A(x, sigma, m);
    auto wektor_b = Konstrukcja_Wektora_b(y, sigma);
    auto macierz_A_tr = transponowanie(macierz_A);
    auto macierz_alfa = mnozenie_macierzy(macierz_A_tr, macierz_A);
    auto wektor_beta = macierz_przez_wektor(macierz_A_tr, wektor_b);
    auto copy_alfa = macierz_alfa;

    metoda_doolittla_permutacja(macierz_permutacji, macierz_L, macierz_U, copy_alfa);


    auto Ly = podstawianie_wprzod(macierz_L, wektor_beta);
    auto Ux = podstawianie_wstecz(macierz_U, Ly);
    auto p_trans = transponowanie(macierz_permutacji);
    auto wynik = macierz_przez_wektor(p_trans, Ux);

    cout << "Macierz alfa: " << endl;
    show_macierz(macierz_alfa);

    cout << "Macierz alfa(odwrocona): " << endl;
    // odwrócone macierze L , U oraz A
    auto macierz_L_odwrocona = odwracanie(macierz_L, macierz_jednostkowa, false);
    auto macierz_U_odwrocona = odwracanie(macierz_U, macierz_jednostkowa, true);

    auto macierz_PU_odwrocone = mnozenie_macierzy(p_trans, macierz_U_odwrocona);
    auto macierz_alfa_odwrocona = mnozenie_macierzy(macierz_PU_odwrocone, macierz_L_odwrocona);
    show_macierz(macierz_alfa_odwrocona);


    cout << "Wektor beta: " << endl;
    show_wektor(wektor_beta);

    cout << "Spodziewany wynik: " << endl;
    cout << "4 3 2 1" << endl;
    // Wynik oblicze
    cout << "Wynik aproksymacji: " << endl;
    show_wektor(wynik);

    zwroc_wartosci_wariancji_kowariancji(macierz_alfa_odwrocona);
    auto x_norm_definicja = norma_macierzy(macierz_alfa_odwrocona) * norma_macierzy(macierz_alfa);
    cout << "Wskaznik uwarunkowania macierzy alfa (z definicji): " << x_norm_definicja << endl;

    return 0;
}