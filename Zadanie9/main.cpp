#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <fstream>
#include <cmath>

using namespace std;

void split(string str, vector<vector<double>> &out_vector) {
    vector<double> values;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, ' ')) {
        if(!tok.empty())
            values.push_back(stod(tok));
    }
    out_vector.push_back(values);
}

template <typename T>
T max_element(vector<T> wiersz){
    T tmp = 0;
    for(int i = 0; i < wiersz.size(); ++i){
        T wartosc = abs(wiersz[i]);
        if (wartosc > tmp) tmp = wartosc;
    }
    return tmp;
}

template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    for(int i = 0; i < n; ++i){
        for(int j = 0 ; j < n; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
}

template <typename T>
void show_wektor(const vector<T> wektor){
    size_t n = wektor.size();
    for(int i = 0; i < n; ++i){
        cout << wektor[i] << " ";
    }
    cout << endl;
}

template <typename T>
void fill_macierz_jednostkowa(vector<vector<T>> &macierz, int n){
    for(int i = 0; i < n; ++i){
        macierz[i].at(i) = 1;
    }
}

static bool abs_compare(double a, double b)
{
    return (abs(a) < abs(b));
}

void swap_kolumny(vector<vector<double>> &macierz, int kolumna1, int kolumna2){

    size_t size = macierz.size();
    for(int i = 0; i < size; ++i){
        double tmp = macierz[i].at(kolumna2);
        macierz[i].at(kolumna2) = macierz[i].at(kolumna1);
        macierz[i].at(kolumna1) = tmp;
    }
}

void swap_row(vector<vector<double>> &macierz, int wiersz1, int wiersz2){

    auto wiersz_tmp = macierz[wiersz1];
    macierz[wiersz1] = macierz[wiersz2];
    macierz[wiersz2] = wiersz_tmp;
}

int maksymalna_wartosc_bezwzgledna_wiersza(vector<vector<double>> macierz, int n){

    auto max2 = max_element(macierz[n].begin(), macierz[n].end(), abs_compare);
    int index = distance(macierz[n].begin(), max2);
    return index;
}

void metoda_doolittla_permutacja(vector<vector<double>> &macierz_permutacji, vector<vector<double>> &macierz_L, vector<vector<double>> &macierz_U, vector<vector<double>>  &macierz_A){
    int n = macierz_A.size();
    for(int k = 0; k < n; k++) {
        // -- wyznaczanie k-tego wierza macierzy U
        for (int j = k; j < n; ++j) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            macierz_U[k].at(j) = macierz_A[k].at(j) - suma;
        }

        int index = maksymalna_wartosc_bezwzgledna_wiersza(macierz_U, k);
        if(k != index) {
            swap_row(macierz_permutacji, k, index);
            swap_kolumny(macierz_U, k, index);
            swap_kolumny(macierz_A, k, index);
        }

        // -- wyznaczanie k-tej kolumny macierzy L
        for (int i = k+1; i < n; ++i) {
            double suma = 0;
            for (int p = 0; p < k; ++p) {
                suma += macierz_L[i].at(p) * macierz_U[p].at(k);
            }
            macierz_L[i].at(k) = (macierz_A[i].at(k) - suma) / macierz_U[k].at(k);
        }

    }
}

template <typename T>
vector<T> macierz_przez_wektor(vector<vector<T>> macierz, vector<T> wektor){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    vector<T> wynik(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < m; ++j){
            suma += macierz[i].at(j) * wektor[j];
        }
        wynik[i] = suma;
    }
    return wynik;
}

template <typename T>
vector<vector<T>> macierz_przez_skalar(vector<vector<T>> macierz, T skalar){
    int n = macierz[0].size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            result_macierz[i].at(j) = macierz[i].at(j) * skalar;
        }
    }
    return result_macierz;
}

template <typename T>
vector<T> wektor_przez_skalar(vector<T> wektor, T skalar){
    int n = wektor.size();
    vector<T> result_wektor(n);
    for(int i = 0; i < n; ++i){
        result_wektor[i] = wektor[i] * skalar;
    }
    return result_wektor;
}

template <typename T>
vector<T> podstawianie_wprzod(vector<vector<T>> macierz_L, vector<T> wektor_b){

    size_t n = macierz_L.size();
    vector<T> wektor_y(n);
    for(int i = 0; i < n; ++i){
        T suma = 0;
        for(int j = 0; j < i; ++j){
            suma += macierz_L[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/ macierz_L[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<T> podstawianie_wstecz(vector<vector<T>> macierz_U, vector<T> wektor_b){

    size_t n = macierz_U.size();
    vector<T> wektor_y(n);
    for(int i = n-1; i >= 0; --i){
        T suma = 0;
        for(int j = i+1; j < n; ++j){
            suma += macierz_U[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (wektor_b[i] - suma)/macierz_U[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<T> podstawianie_wstecz_zadanie9(vector<vector<T>> macierz_U){

    size_t n = macierz_U.size();
    vector<T> wektor_y(n);
    wektor_y[n-1] = 1;
    for(int i = n-2; i >= 0; --i){
        T suma = 0.0;
        for(int j = i+1; j < n; ++j){
            suma += macierz_U[i].at(j) * wektor_y[j];
        }
        wektor_y[i] = (0.0 - suma)/macierz_U[i].at(i);
    }
    return wektor_y;
}

template <typename T>
vector<vector<T>> odwracanie(vector<vector<T>> macierz, vector<vector<T>> macierz_jednostkowa, bool upper){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    vector<vector<T>> macierz_odwrotna(n, vector<T>(m));
    for(int k = 0; k < n; ++k){
        auto e = get_kolumna(macierz_jednostkowa, k);
        auto y = upper? podstawianie_wstecz(macierz, e) : podstawianie_wprzod(macierz, e);
        for(int i = 0; i < m; ++i){
            macierz_odwrotna[i].at(k) = y[i];
        }
    }
    return macierz_odwrotna;
}

template <typename T>
vector<vector<T>> transponowanie(vector<vector<T>> macierz){
    int n = macierz.size();
    int m = macierz[0].size();
    vector<vector<T>> tmp(m, vector<T>(n));
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            tmp[i].at(j) = macierz[j].at(i);
        }
    }
    return tmp;
}

template <typename T>
T norma_macierzy(vector<vector<T>> macierz){
    vector<T> values;
    for(int j = 0; j < macierz.size(); ++j){
        T suma = 0;
        for(int i = 0; i < macierz[0].size(); ++i){
            suma += abs(macierz[i].at(j));
        }
        values.push_back(suma);
    }
    return max_element(values);
}

template <typename T>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> macierz_L, vector<vector<T>> macierz_U){
    size_t n = macierz_L.size();
    size_t m = macierz_L[0].size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        for(int j = 0; j < n; ++j){
            T suma = 0;
            for(int p = 0; p < m; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    return result_macierz;
}

template <typename T>
vector<vector<T>> odejmowanie_macierzy(vector<vector<T>> macierz1, vector<vector<T>> macierz2, int n){

    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            result_macierz[i].at(j) = macierz1[i].at(j) - macierz2[i].at(j);
        }
    }
    return result_macierz;
}

template <typename T>
vector<vector<T>> dodawanie_macierzy(vector<vector<T>> macierz1, vector<vector<T>> macierz2, int n){

    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            result_macierz[i].at(j) = macierz1[i].at(j) + macierz2[i].at(j);
        }
    }
    return result_macierz;
}

template <typename T>
T norma_euklidesowa(vector<T> wektor){
    T suma = 0;
    for(int i = 0; i < wektor.size(); ++i){
        suma += pow(wektor[i], 2);
    }
    return sqrt(suma);
}

template <typename T>
vector<T> normalizacja_wektora(vector<T> wektor){
    int n = wektor.size();
    vector<T> wektor_znormalizowany(n);
    T norma_wektora_tmp = norma_euklidesowa(wektor);
    for(int i = 0; i < n; ++i){
        wektor_znormalizowany[i] = wektor[i] / norma_wektora_tmp;
    }
    return wektor_znormalizowany;
}

int main(int argc, char** argv) {

    int n = 0;
    ifstream file;
    stringstream filename_macierz, filename_wlasne;
    filename_macierz << "macierz" << argv[1] << ".txt";
    filename_wlasne << "wlasne" << argv[1] << ".txt";

    // można wybrać spośród macierz 1, 2 oraz 3
    file.open(filename_macierz.str());
    string line;
    vector<vector<double>> macierz_A;
    while (getline(file, line)){
        split(line, macierz_A);
        if(n == 0) n = macierz_A[0].size();
    }

    file.close();
    vector<double> wartosci_wlasne;
    file.open(filename_wlasne.str());
    double tmp_input;
    while(file >> tmp_input){
        wartosci_wlasne.push_back(tmp_input);
    }

    vector<vector<double>> macierz_jednostkowa(n, vector<double>(n));
    fill_macierz_jednostkowa(macierz_jednostkowa, n);
    vector<double> wektor_zerowy(n);


    for(int i = 0; i < n; ++i){
        cout << "Iteracja " << i+1 << " dla wartosci wlasnej = " << wartosci_wlasne[i] << endl;
        auto tmp_wynik = macierz_przez_skalar(macierz_jednostkowa, wartosci_wlasne[i]);
        vector<vector<double>> macierz_A_i = odejmowanie_macierzy(macierz_A, tmp_wynik, n);
        vector<vector<double>> macierz_L = macierz_jednostkowa;
        vector<vector<double>> macierz_U(n, vector<double>(n));
        vector<vector<double>> macierz_P = macierz_jednostkowa;

        //cout << "Macierz A_lambda: " << endl;
        //show_macierz(macierz_A_i);

        metoda_doolittla_permutacja(macierz_P, macierz_L, macierz_U, macierz_A_i);
        auto x_i_prim = podstawianie_wstecz_zadanie9(macierz_U);
        auto macierz_P_transponowana = transponowanie(macierz_P);
        auto wektor_wlasny_i = macierz_przez_wektor(macierz_P_transponowana, x_i_prim);
        cout << "Wektor wlasny " << i+1 << " : " << endl;
        show_wektor(wektor_wlasny_i);

        cout << "lambda[i]*x[i]: " << endl;
        show_wektor(wektor_przez_skalar(wektor_wlasny_i, wartosci_wlasne[i]));

        cout << "A*x[i]: " << endl;
        show_wektor(macierz_przez_wektor(macierz_A, wektor_wlasny_i));

        cout << "Wektor wlasny znormalizowany: " << endl;
        show_wektor(normalizacja_wektora(wektor_wlasny_i));



        cout << endl;
    }

    return 0;
}