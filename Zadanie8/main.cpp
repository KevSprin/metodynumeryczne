#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

template <typename T>
bool macierze_podobne(vector<vector<T>> mat1, vector<vector<T>> mat2){
    int n = mat1.size();
    for(int i = 0 ; i < n; ++i){
        for(int j = 0; j < n; ++j){
            if(mat1[i].at(j) != mat2[i].at(j)) return false;
        }
    }
    return true;
}

template <typename T>
bool podobne_diagonalne(vector<vector<T>> mat1, vector<vector<T>> mat2){
    int n = mat1.size();
    for(int i = 0; i < n; ++i){
        if(mat1[i].at(i) != mat2[i].at(i)) return false;
    }
    return true;
}

template <typename T>
bool jest_trojkatna(vector<vector<T>> mat){
    int n = mat.size();
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            if(i > j){
                if(mat[i].at(j) != 0) return false;
            }
        }
    }
    return true;
}

template <typename T>
vector<T> get_kolumna(vector<vector<T>> macierz, int index){
    int n = macierz.size();
    vector<T> kolumna(n);
    for(int i = 0; i < n; ++i){
        kolumna[i] = macierz[i].at(index);
    }
    return kolumna;
}

template <typename T>
void insert_kolumna(vector<vector<T>> &macierz, vector<T> kolumna, int index){
    int n = macierz.size();
    for(int i = 0; i < n; ++i){
        macierz[i].at(index) = kolumna[i];
    }
}

void split(string str, vector<vector<double>> &out_vector) {
    vector<double> values;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, ' ')) {
        if(!tok.empty())
            values.push_back(stod(tok));
    }
    out_vector.push_back(values);
}

template<typename T>
void show_macierz(const vector<vector<T>> &macierz){
    size_t n = macierz.size();
    size_t m = macierz[0].size();
    for(int i = 0; i < n; ++i){
        for(int j = 0 ; j < m; ++j){
            cout << macierz[i].at(j) << " ";
        }
        cout << endl;
    }
    cout << endl;
}

template <typename T>
T iloczyn_skalarny_wektorow(vector<T> v1, vector<T> v2){
    T result = 0;
    for(int i = 0; i < v1.size(); ++i){
        result += v1[i] * v2[i];
    }
    return result;
}

template <typename T>
vector<T> wektor_przez_skalar(vector<T> wektor, T skalar){
    for(int i = 0; i < wektor.size(); ++i){
        wektor[i] *= skalar;
    }
    return wektor;
}

template <typename T>
vector<T> wektor_odjac_wektor(vector<T> v1, vector<T> v2){
    int n = v1.size();
    vector<T> result(n);
    for(int i = 0; i < n; ++i){
        result[i] = v1[i] - v2[i];
    }
    return result;
}

template <typename T>
vector<T> wektor_dodac_wektor(vector<T> v1, vector<T> v2){
    int n = v1.size();
    vector<T> result(n);
    for(int i = 0; i < n; ++i){
        result[i] = v1[i] + v2[i];
    }
    return result;
}

template <typename T>
T norma_euklidesowa(vector<T> wektor){
    T suma = 0;
    for(int i = 0; i < wektor.size(); ++i){
        suma += pow(wektor[i], 2);
    }
    return sqrt(suma);
}

template <typename T>
vector<vector<T>> mnozenie_macierzy(vector<vector<T>> macierz_L, vector<vector<T>> macierz_U){
    size_t n = macierz_L.size();
    size_t m = macierz_L[0].size();
    vector<vector<T>> result_macierz(n, vector<T>(n));
    for(int k = 0; k < n; ++k){
        for(int j = 0; j < n; ++j){
            T suma = 0;
            for(int p = 0; p < m; ++p){
                suma += macierz_L[k].at(p) * macierz_U[p].at(j);
            }
            result_macierz[k].at(j) = suma;
        }
    }
    return result_macierz;
}

template <typename T>
vector<T> normalizacja_wektora(vector<T> wektor){
    int n = wektor.size();
    vector<T> wektor_znormalizowany(n);
    T norma_wektora_tmp = norma_euklidesowa(wektor);
    for(int i = 0; i < n; ++i){
        wektor_znormalizowany[i] = wektor[i] / norma_wektora_tmp;
    }
    return wektor_znormalizowany;
}

template <typename T>
void print_wartosci_wlasne(vector<vector<T>> mat){
    cout << "Wartosci wlasne: ";
    for(int i = 0; i < mat.size(); ++i){
        cout << mat[i].at(i) << " ";
    }
}

template <typename T>
void rozklad_QR(vector<vector<T>> macierz_A, vector<vector<T>> &macierz_Q, vector<vector<T>> &macierz_R){

    int n = macierz_A.size();
    vector<vector<T>> A_prim(n, vector<T>(n));

    // tworzenie macierzy Q (ortogonalna)
    for(int i = 0; i < n; ++i){
        vector<T> q(n);
        if(i == 0){
            auto a = get_kolumna(macierz_A, i);
            q = normalizacja_wektora(a);
            insert_kolumna(A_prim, a, i);
            insert_kolumna(macierz_Q, q, i);
            continue;
        }
        auto a_i = get_kolumna(macierz_A, i);

        vector<T> sum(n);
        for(int k = 0; k < i; ++k){
            auto q_k = get_kolumna(macierz_Q, k);
            auto qk_ai = iloczyn_skalarny_wektorow(q_k, a_i);
            auto result = wektor_przez_skalar(q_k, qk_ai);
            sum = wektor_dodac_wektor(sum, result);
        }

        auto a_prim = wektor_odjac_wektor(a_i, sum);
        insert_kolumna(A_prim, a_prim, i);
        q = normalizacja_wektora(a_prim);
        insert_kolumna(macierz_Q, q, i);
    }

    // tworzenie macierzy R (górna trójkątna)
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            if(i == j) macierz_R[i].at(i) = norma_euklidesowa(get_kolumna(A_prim, i));
            else if (i < j) macierz_R[i].at(j) = iloczyn_skalarny_wektorow(get_kolumna(macierz_Q, i), get_kolumna(macierz_A, j));
            else if (i > j) macierz_R[i].at(j) = 0;
        }
    }
}

int main() {

    int n = 0;
    double d;
    ifstream file;
    // można wybrać spośród macierz 1, 2 oraz 3
    file.open("macierz1.txt");
    string line;
    vector<vector<double>> macierz_A;
    while (getline(file, line)){
        split(line, macierz_A);
        if(n == 0) n = macierz_A[0].size();
    }

    vector<vector<double>> macierz_A1 = macierz_A;

    int epoch = 1;
    while(1){
        vector<vector<double>> macierz_Q(n, vector<double>(n));
        vector<vector<double>> macierz_R(n, vector<double>(n));
        rozklad_QR(macierz_A, macierz_Q, macierz_R);

        auto macierz_A_k1 = mnozenie_macierzy(macierz_R, macierz_Q);

        if(macierze_podobne(macierz_A_k1, macierz_A) || podobne_diagonalne(macierz_A_k1, macierz_A) || jest_trojkatna(macierz_A_k1)){
            cout << "Epoch: " << epoch << endl;
            cout << "Macierz A_k: " << endl;
            show_macierz(macierz_A_k1);
            cout << "Macierz A_k+1: " << endl;
            show_macierz(macierz_A);
            cout << "Macierz Q_k: " << endl;
            show_macierz(macierz_Q);
            cout << "Macierz R_k: " << endl;
            show_macierz(macierz_R);
            print_wartosci_wlasne(macierz_A_k1);
            break;
        }
        macierz_A = macierz_A_k1;
        epoch++;
    }
    return 0;
}